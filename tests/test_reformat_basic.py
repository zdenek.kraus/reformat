# Copyright 2017 Zdenek Kraus <zdenek.kraus@gmail.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.import reformat

from unittest import TestCase

import reformat


class TestReformatBasic(TestCase):

    # data tests
    def test_none_data(self):
        s = 'xx %{a} yy'
        d = None
        exp = 'xx %{a} yy'
        x = reformat.reformat(s, d)
        self.assertEqual(exp, x)

    def test_none(self):
        s = 'xx %{a} yy'
        d = {'a': None}
        exp = 'xx  yy'
        x = reformat.reformat(s, d)
        self.assertEqual(exp, x)

    def test_empty_data(self):
        s = 'xx %{a} yy'
        d = {}
        exp = 'xx  yy'
        x = reformat.reformat(s, d)
        self.assertEqual(exp, x)

    def test_empty_string(self):
        s = ''
        d = {}
        exp = ''
        x = reformat.reformat(s, d)
        self.assertEqual(exp, x)

    # first level reformat
    def test_a(self):
        s = 'Test %{a}'
        d = {'a': 'AAA'}
        exp = 'Test AAA'
        x = reformat.reformat(s, d)
        self.assertEqual(exp, x)

    def test_a_and_other_data(self):
        s = 'Test %{a}'
        d = {'a': 'AAA', 'b': 'BBB', 'c': 'CCC'}
        exp = 'Test AAA'
        x = reformat.reformat(s, d)
        self.assertEqual(exp, x)

    def test_a_only_other_data(self):
        s = 'Test %{a}'
        d = {'b': 'BBB', 'c': 'CCC'}
        exp = 'Test '
        x = reformat.reformat(s, d)
        self.assertEqual(exp, x)

    # second level reformat
    def test_a_npt(self):
        s = 'Test %{a|}'
        d = {'a': 'AAA'}
        exp = 'Test AAA'
        x = reformat.reformat(s, d)
        self.assertEqual(exp, x)

    def test_a_ptx(self):
        s = 'Test %{a|xxx}'
        d = {'a': 'AAA'}
        exp = 'Test xxx'
        x = reformat.reformat(s, d)
        self.assertEqual(exp, x)

    def test_a_ptx_mis(self):
        s = 'Test %{a|xxx}'
        d = {}
        exp = 'Test '
        x = reformat.reformat(s, d)
        self.assertEqual(exp, x)

    # second level reformat with %s
    def test_a_pts(self):
        s = 'Test %{a|sss %s}'
        d = {'a': 'AAA'}
        exp = 'Test sss AAA'
        x = reformat.reformat(s, d)
        self.assertEqual(exp, x)

    def test_a_pts_mis(self):
        s = 'Test %{a|sss %s}'
        d = {}
        exp = 'Test '
        x = reformat.reformat(s, d)
        self.assertEqual(exp, x)

    # third level reformat
    def test_a_npt_nptn(self):
        s = 'Test %{a||}'
        d = {'a': 'AAA'}
        exp = 'Test AAA'
        x = reformat.reformat(s, d)
        self.assertEqual(exp, x)

    def test_a_npt_nptn_mis(self):
        s = 'Test %{a||}'
        d = {}
        exp = 'Test '
        x = reformat.reformat(s, d)
        self.assertEqual(exp, x)

    def test_a_npt_ptnx(self):
        s = 'Test %{a||xxx}'
        d = {'a': 'AAA'}
        exp = 'Test AAA'
        x = reformat.reformat(s, d)
        self.assertEqual(exp, x)

    def test_a_npt_ptnx_mis(self):
        s = 'Test %{a||xxx}'
        d = {}
        exp = 'Test xxx'
        x = reformat.reformat(s, d)
        self.assertEqual(exp, x)

    def test_a_ptx_ptnx(self):
        s = 'Test %{a|yyy|xxx}'
        d = {'a': 'AAA'}
        exp = 'Test yyy'
        x = reformat.reformat(s, d)
        self.assertEqual(exp, x)

    def test_a_ptx_ptnx_mis(self):
        s = 'Test %{a|yyy|xxx}'
        d = {}
        exp = 'Test xxx'
        x = reformat.reformat(s, d)
        self.assertEqual(exp, x)

    # third level reformat with %s
    def test_a_pts_ptns(self):
        s = 'Test %{a|yyy %s zy|xxx %s zx}'
        d = {'a': 'AAA'}
        exp = 'Test yyy AAA zy'
        x = reformat.reformat(s, d)
        self.assertEqual(exp, x)

    def test_a_pts_ptns_mis(self):
        s = 'Test %{a|yyy %s zy|xxx %s zx}'
        d = {}
        exp = 'Test xxx  zx'
        x = reformat.reformat(s, d)
        self.assertEqual(exp, x)

    # defaults tests
    def test_a_pts_ptns_default(self):
        s = 'Test %{a|yyy %s zy|xxx %s zx}'
        d = {'a': 'AAA'}
        default = 'DDD'
        exp = 'Test yyy AAA zy'
        x = reformat.reformat(s, d, default)
        self.assertEqual(exp, x)

    def test_a_pts_ptns_mis_default(self):
        s = 'Test %{a|yyy %s zy|xxx %s zx}'
        d = {}
        default = 'DDD'
        exp = 'Test xxx DDD zx'
        x = reformat.reformat(s, d, default)
        self.assertEqual(exp, x)

    # special cases
    def test_percent_s(self):
        s = 'xx %s yy'
        d = {'a': 'AAA'}
        exp = 'xx %s yy'
        x = reformat.reformat(s, d)
        self.assertEqual(exp, x)

    def test_a_percent_s(self):
        s = 'xx %{a} %s yy'
        d = {'a': 'AAA'}
        exp = 'xx AAA %s yy'
        x = reformat.reformat(s, d)
        self.assertEqual(exp, x)

    def test_a_mis_percent_s(self):
        s = 'xx %{a} %s yy'
        d = {}
        exp = 'xx  %s yy'
        x = reformat.reformat(s, d)
        self.assertEqual(exp, x)

    def test_a_pts_percent_s(self):
        s = 'xx %{a|asdf %s} %s yy'
        d = {'a': 'AAA'}
        exp = 'xx asdf AAA %s yy'
        x = reformat.reformat(s, d)
        self.assertEqual(exp, x)
