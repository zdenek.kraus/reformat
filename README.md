# Text reformat utility #

For contextual text formatting in python. Very simple markup
that allows to format/subtitute text depending on provided data.

Instead of doing:

```python
name = "John Doe"
title = ""

txt = "Dear %s," % name
if title:
    txt = "Dear %s %s," % (name, title)
```

with reformat you can do:

```python
import reformat

data = {
    'name': 'John Doe',
    'title': 'Mr.',
    'address': 'Super Street 1, City Center',
}

txt = reformat.reformat("Dear %{title|%s }%{name}", data)
# Dear Mr. John Doe,
```

And reformat will substitute the pattern with provided data.

There is also negative expression if needed:

```python
import reformat

data = {
    'master': 'master.example.com',
    # 'slave': 'slave.example.com, # is deliberatively missing
}

pattern = "Connect to %{master|%s} %{slave|and %s|only}."

txt = reformat.reformat(pattern, data)
# "Connect to master.example.com only."
```

You don't even need a formatting `%s` there, just some static text
formatted in case of true value in provided data.

### Contribution guidelines ###

If you would like to contribute, keep in mind, that any new additions should be generic, if you need to add specific behavior to you application, better way would be to inherit existing and add your behavior to your project.


### Contributors ###

* Zdenek Kraus <zdenek.kraus@gmail.com>

### License ###

Copyright 2017 Zdenek Kraus <zdenek.kraus@gmail.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitati